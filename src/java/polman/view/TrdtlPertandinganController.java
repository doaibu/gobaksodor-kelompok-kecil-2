package polman.view;

import polman.model.TrdtlPertandingan;
import polman.view.util.JsfUtil;
import polman.view.util.PaginationHelper;
import polman.controller.TrdtlPertandinganFacade;

import java.io.Serializable;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;

@Named("trdtlPertandinganController")
@SessionScoped
public class TrdtlPertandinganController implements Serializable {

    private TrdtlPertandingan current;
    private DataModel items = null;
    @EJB
    private polman.controller.TrdtlPertandinganFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;

    public TrdtlPertandinganController() {
    }

    public TrdtlPertandingan getSelected() {
        if (current == null) {
            current = new TrdtlPertandingan();
            current.setTrdtlPertandinganPK(new polman.model.TrdtlPertandinganPK());
            selectedItemIndex = -1;
        }
        return current;
    }

    private TrdtlPertandinganFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (TrdtlPertandingan) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new TrdtlPertandingan();
        current.setTrdtlPertandinganPK(new polman.model.TrdtlPertandinganPK());
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
        try {
            current.getTrdtlPertandinganPK().setDtsIdjad(current.getTrJadwal().getJadId());
            current.getTrdtlPertandinganPK().setDtsIdpem(current.getMsPeserta().getPesId());
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("TrdtlPertandinganCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (TrdtlPertandingan) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            current.getTrdtlPertandinganPK().setDtsIdjad(current.getTrJadwal().getJadId());
            current.getTrdtlPertandinganPK().setDtsIdpem(current.getMsPeserta().getPesId());
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("TrdtlPertandinganUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (TrdtlPertandingan) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("TrdtlPertandinganDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public TrdtlPertandingan getTrdtlPertandingan(polman.model.TrdtlPertandinganPK id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = TrdtlPertandingan.class)
    public static class TrdtlPertandinganControllerConverter implements Converter {

        private static final String SEPARATOR = "#";
        private static final String SEPARATOR_ESCAPED = "\\#";

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            TrdtlPertandinganController controller = (TrdtlPertandinganController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "trdtlPertandinganController");
            return controller.getTrdtlPertandingan(getKey(value));
        }

        polman.model.TrdtlPertandinganPK getKey(String value) {
            polman.model.TrdtlPertandinganPK key;
            String values[] = value.split(SEPARATOR_ESCAPED);
            key = new polman.model.TrdtlPertandinganPK();
            key.setDtsIdjad(Integer.parseInt(values[0]));
            key.setDtsIdpem(Integer.parseInt(values[1]));
            return key;
        }

        String getStringKey(polman.model.TrdtlPertandinganPK value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value.getDtsIdjad());
            sb.append(SEPARATOR);
            sb.append(value.getDtsIdpem());
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof TrdtlPertandingan) {
                TrdtlPertandingan o = (TrdtlPertandingan) object;
                return getStringKey(o.getTrdtlPertandinganPK());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + TrdtlPertandingan.class.getName());
            }
        }

    }

}
