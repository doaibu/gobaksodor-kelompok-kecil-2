package polman.view;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import polman.model.TrBerita;
import polman.view.util.JsfUtil;
import polman.view.util.PaginationHelper;
import polman.controller.TrBeritaFacade;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import polman.controller.TrGambarBeritaFacade;
import polman.model.TrGambarBerita;

@Named("trBeritaController")
@SessionScoped
public class TrBeritaController implements Serializable {

    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("GobakSodorPU");
    private EntityManager em = emf.createEntityManager();
    private int idberita,idBeritaDetail;
    private TrBerita current;
    private DataModel items = null;
    @EJB
    private polman.controller.TrBeritaFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;
    boolean hapus;

    public int getIdBeritaDetail() {
        return idBeritaDetail;
    }

    public TrBeritaFacade getEjbFacade() {
        return ejbFacade;
    }

    public void setEjbFacade(TrBeritaFacade ejbFacade) {
        this.ejbFacade = ejbFacade;
    }
    
    
    public void setIdBeritaDetail(int idBeritaDetail) {
        this.idBeritaDetail = idBeritaDetail;
    }
    
    public String baca()
    {
        current = ejbFacade.find(idBeritaDetail);
        current.setBerPengunjung(current.getBerPengunjung()+1);
        ejbFacade.edit(current);
        return "bacaberita";
    }
    public boolean isHapus() {
        return hapus;
    }

    public void setHapus(boolean hapus) {
        this.hapus = hapus;
    }
    public List getFullBerita()
    {
        List axc = ejbFacade.getAllBerita();
        return axc;
    }
    public TrBeritaController() {
    }
    
    public List getBeritaDetail()
    {
        List a = ejbFacade.getBeritaDetail(idBeritaDetail);
        
        return ejbFacade.getBeritaDetail(idBeritaDetail);
    }
    
    
    public List getDaftarBerita()
    {
        List a = ejbFacade.findAll();
        
        return a;
    }
    public List getHighLight()
    {
        List a = ejbFacade.getAllBerita();
        return ejbFacade.getAllBerita();
    }
    
    public List getPopuler()
    {
        List a = ejbFacade.getAllPopuler();
        List b = new ArrayList();
        if(a.size()>5)
        {
            for(int x=0;x<a.size();x++)
            {
                if(x==5)
                {
                    return b;
                }
                b.add(a.get(x));
            }
        }
            
        return ejbFacade.getAllPopuler();
    }
    public List getKomentarBanyak()
    {
        List a = ejbFacade.getBeritaKomentar();
        List b = new ArrayList();
        if(a.size()>5)
        {
            for(int x=0;x<a.size();x++)
            {
                if(x==5)
                {
                    return b;
                }
                b.add(a.get(x));
            }
        }
            
        return ejbFacade.getBeritaKomentar();
    }
    public List getBeritaTerbaru() throws ParseException
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar tgl =  Calendar.getInstance();
        tgl =Calendar.getInstance();
        tgl.add(Calendar.DATE,-7);
        Date sekarang = null;
        sekarang = tgl.getTime();
        String tanggal = sdf.format(sekarang);
        sekarang = sdf.parse(tanggal);
        List a = ejbFacade.getBeritaLast(sekarang);
        List b = new ArrayList();
        if(a.size()>5)
        {
            for(int x=0;x<a.size();x++)
            {
                if(x==5)
                {
                    return b;
                }
                b.add(a.get(x));
            }
        }
            
        return ejbFacade.getBeritaLast(sekarang);
    }
    public TrBerita getSelected() {
        if (current == null) {
            current = new TrBerita();
            selectedItemIndex = -1;
        }
        return current;
    }

    public int getIdberita() {
        return idberita;
    }

    public void setIdberita(int idberita) {
        this.idberita = idberita;
    }
    
    
    

    private TrBeritaFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public List getAllBerita()
    {
        List<TrBerita> ListA= ejbFacade.findAll();
        for(int a=0;a<ListA.size();a++)
        {
            String kalimat = ListA.get(a).getBerKonten();
            String[] kontenberita = kalimat.split("\\.");
            ListA.get(a).setBerKonten(kontenberita[0]+"."+kontenberita[1]+".");
            
        }
        return ListA;
    }
    
    public String prepareView() {
        current = (TrBerita) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new TrBerita();
        selectedItemIndex = -1;
        return "CreateGambar.xhtml";
    }
    private transient StreamedContent file;

    public StreamedContent getFile() {
        InputStream in = new ByteArrayInputStream(current.getBerKonten().getBytes());
        this.file = new DefaultStreamedContent(in, "text/html", "result.html");
        return file;
    }
    TrBerita idBerita;

    public TrBerita getIdBerita() {
        return idBerita;
    }

    public void setIdBerita(TrBerita idBerita) {
        this.idBerita = idBerita;
    }
    

    public String create() {
        try {
            String a = current.getBerKonten();
            idBerita = current;
            String kalimat = current.getBerKonten();
            String[] kontenberita = kalimat.split("\\. ");
            current.setBerResume(kontenberita[0]+"."+kontenberita[1]+".");
            Date sekarang = new Date();
            current.setBerTglRilis(sekarang);
            current.setBerPengunjung(0);
            getFacade().create(current);
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }
    private List<TrGambarBerita> list;

    public List <TrGambarBerita> getList(){
//        String prodi = (String.valueOf( em.createQuery("SELECT u.prodi FROM Kelas u WHERE u.kodeKelas =:kode")
//                .setParameter("kode",id)
//                .getSingleResult()));
        try{
            int id = idberita;
        List<TrGambarBerita> x = (( em.createQuery("SELECT u FROM TrGambarBerita u WHERE u.gbrBerita.berId = :idberita")
                .setParameter("idberita",idberita)
                .getResultList()));
//        gbBerita = (TrGambarBerita) x;
        return x;
        }catch(Exception e){
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
        
    }

    public void setList(List<TrGambarBerita> list) {
        this.list = list;
    }

    public String prepareEdit() {
        hapus=false;
        current = ejbFacade.find(idberita);
//        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        getList();
        return "Edit";
    }
    
    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("TrBeritaUpdated"));
            return "List";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = ejbFacade.find(idberita);
        getFacade().remove(current);
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("TrBeritaDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }
    
    
    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public TrBerita getTrBerita(java.lang.Integer id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = TrBerita.class)
    public static class TrBeritaControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            TrBeritaController controller = (TrBeritaController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "trBeritaController");
            return controller.getTrBerita(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof TrBerita) {
                TrBerita o = (TrBerita) object;
                return getStringKey(o.getBerId());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + TrBerita.class.getName());
            }
        }

    }

}
