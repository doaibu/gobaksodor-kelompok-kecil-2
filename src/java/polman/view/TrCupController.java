package polman.view;

import java.io.IOException;
import polman.model.TrCup;
import polman.view.util.JsfUtil;
import polman.view.util.PaginationHelper;
import polman.controller.TrCupFacade;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.application.NavigationHandler;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

@Named("trCupController")
@SessionScoped
public class TrCupController implements Serializable {

    private TrCup current;
    private DataModel items = null;
    @EJB
    private polman.controller.TrCupFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;

    public TrCupController() {
    }

    
    public TrCup getSelected() {
        if (current == null) {
            current = new TrCup();
            selectedItemIndex = -1;
        }
        return current;
    }
    Date min;

    public Date getMin() {
        return min;
    }

    public void setMin(Date min) {
        this.min = min;
    }
    public void onDateSelect(SelectEvent event) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        SimpleDateFormat format = new SimpleDateFormat("dd MMMMM yyyy");
        try {
            min = format.parse( format.format(event.getObject()));
        } catch (ParseException ex) {
            Logger.getLogger(TrCupController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private TrCupFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (TrCup) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new TrCup();
        selectedItemIndex = -1;
        return "List";
    }

    public String create() {
        try {
            
            getFacade().create(current);
            current = new TrCup();
            selectedItemIndex = -1;
            return "List";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }
    boolean showAlert = false;

    public boolean isShowAlert() {
        return showAlert;
    }

    public void setShowAlert(boolean showAlert) {
        this.showAlert = showAlert;
    }
    Date date1;

    public Date getDate1() {
        return date1;
    }

    public void setDate1(Date date1) {
        this.date1 = date1;
    }
    int idCup;

    public int getIdCup() {
        return idCup;
    }

    public void setIdCup(int idCup) {
        this.idCup = idCup;
    }
    
    public String prepareEdit() throws IOException {
//        current = (TrCup) getItems().getRowData();
        current = ejbFacade.find(idCup);
        hapus=false;
        Date a = new Date();
        if(current.getCupMulai().after(a))
        {
            showAlert = false;
            return "Edit.xhtml?faces-redirect=true";
//            FacesContext facesContext = FacesContext.getCurrentInstance();
//            String redirect = "Edit";
//            NavigationHandler myNav = facesContext.getApplication().getNavigationHandler();
//            myNav.handleNavigation(facesContext, null, redirect);
        }
        else
        {
            
            showAlert = true;
            
            return null;
            
        }
        
         
        //return "Edit";
    }

    public String update() {
        try {
            getFacade().edit(current);
            current = new TrCup();
            selectedItemIndex = -1;
            return "List";
        } catch (Exception e) {
            return null;
        }
    }
    boolean hapus = false;

    public boolean isHapus() {
        return hapus;
    }

    public void setHapus(boolean hapus) {
        this.hapus = hapus;
    }
    
    public String destroy() {
        current = ejbFacade.find(idCup);
        Date a = new Date();
        showAlert = false;
        if(current.getCupMulai().after(a))
        {
            
            getFacade().remove(current);
            recreatePagination();
            recreateModel();
            hapus = false;
            
        }
        else
        {
            
            hapus = true;
            return null;
            
        }
        
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("TrCupDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }
    public List getDaftarKompetisi()
    {
        List a = ejbFacade.findAll();
        return a;
    }
    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public TrCup getTrCup(java.lang.Integer id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = TrCup.class)
    public static class TrCupControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            TrCupController controller = (TrCupController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "trCupController");
            return controller.getTrCup(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof TrCup) {
                TrCup o = (TrCup) object;
                return getStringKey(o.getCupId());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + TrCup.class.getName());
            }
        }

    }

}
