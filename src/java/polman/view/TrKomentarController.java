package polman.view;

import polman.model.TrKomentar;
import polman.model.TrBerita;
import polman.view.util.JsfUtil;
import polman.view.util.PaginationHelper;
import polman.controller.TrKomentarFacade;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import polman.controller.TrBeritaFacade;
import polman.model.TrGambarBerita;

@Named("trKomentarController")
@SessionScoped
public class TrKomentarController implements Serializable {

    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("GobakSodorPU");
    private EntityManager em = emf.createEntityManager();
    private int idberita;
    private TrBerita berita;
    private TrKomentar current;
    private DataModel items = null;
    @EJB
    private polman.controller.TrKomentarFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;
    private int idDetailKomentar;

    public int getIdDetailKomentar() {
        return idDetailKomentar;
    }

    public void setIdDetailKomentar(int idDetailKomentar) {
        this.idDetailKomentar = idDetailKomentar;
    }
    
    public List getKomentarlevelsatu()
    {
        List a = ejbFacade.getKomentarSatu(idDetailKomentar);
        return a;
    }
    public List getKomentarlevelanak(int id)
    {
        List a = ejbFacade.getKomentar(id);
        return a;
    }
    public List getAnakKomentar(int id)
    {
        List a = ejbFacade.getKomentar(id);
        return a;
    }
    public TrKomentarController() {
    }
    public TrKomentar getSelected() {
        if (current == null) {
            current = new TrKomentar();
            selectedItemIndex = -1;
        }
        return current;
    }

    private TrKomentarFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public int getIdberita() {
        return idberita;
    }

    public void setIdberita(int idberita) {
        this.idberita = idberita;
    }
    
    public void tampilkomen() {
        
    }

    private List<TrGambarBerita> gbrberita() {
        try {
            int id = idberita;
            List<TrGambarBerita> x = ((em.createQuery("SELECT u FROM TrGambarBerita u WHERE u.gbrBerita.berId = :idberita")
                    .setParameter("idberita", idberita)
                    .getResultList()));
            return x;
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }
    
    private List<TrBerita> getberita() {
        try {
            int id = idberita;
            List<TrBerita> x = ((em.createQuery("SELECT u FROM TrBerita u WHERE u.berId = :idberita")
                    .setParameter("idberita", idberita)
                    .getResultList()));
            return x;
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public TrBerita getBerita() {
        return berita;
    }

    public void setBerita(TrBerita berita) {
        this.berita = berita;
    }

    public List<TrGambarBerita> getList() {
        try {
            int id = idberita;
            List<TrGambarBerita> x = ((em.createQuery("SELECT u FROM TrGambarBerita u WHERE u.gbrBerita.berId = :idberita")
                    .setParameter("idberita", idberita)
                    .getResultList()));
            return x;
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }

    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (TrKomentar) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new TrKomentar();
        selectedItemIndex = -1;
        return "Create";
    }
    TrBerita beritaKomen = new TrBerita();

    public TrBerita getBeritaKomen() {
        return beritaKomen;
    }

    public void setBeritaKomen(TrBerita beritaKomen) {
        this.beritaKomen = beritaKomen;
    }
    TrBeritaFacade ejbBerita = new TrBeritaFacade();

    public TrBeritaFacade getEjbBerita() {
        return ejbBerita;
    }

    public void setEjbBerita(TrBeritaFacade ejbBerita) {
        this.ejbBerita = ejbBerita;
    }
    
    String idParent = "";
    String idBeritaJawab = "";

    public String getIdBeritaJawab() {
        return idBeritaJawab;
    }

    public void setIdBeritaJawab(String idBeritaJawab) {
        this.idBeritaJawab = idBeritaJawab;
    }
    
    public String getIdParent() {
        return idParent;
    }

    public void setIdParent(String idParent) {
        this.idParent = idParent;
    }
    
    public String create() {
        try {
//            getFacade().create(current);
            beritaKomen = new TrBerita();
            current.setKomIsi(current.getKomIsi());
            beritaKomen = ejbBerita.find(idDetailKomentar);
            current.setKomBerita(beritaKomen);
            getFacade().create(current);
            current = new TrKomentar();
            return "bacaberita";
        } catch (Exception e) {
            return null;
        }
    }
    

    public String prepareEdit() {
        current = (TrKomentar) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle/baru").getString("TrKomentarUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle/baru").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (TrKomentar) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle/baru").getString("TrKomentarDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle/baru").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public TrKomentar getTrKomentar(java.lang.Integer id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = TrKomentar.class)
    public static class TrKomentarControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            TrKomentarController controller = (TrKomentarController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "trKomentarController");
            return controller.getTrKomentar(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof TrKomentar) {
                TrKomentar o = (TrKomentar) object;
                return getStringKey(o.getKomId());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + TrKomentar.class.getName());
            }
        }

    }

}
