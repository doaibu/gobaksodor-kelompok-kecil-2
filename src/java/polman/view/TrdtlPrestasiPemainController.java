package polman.view;

import polman.model.TrdtlPrestasiPemain;
import polman.view.util.JsfUtil;
import polman.view.util.PaginationHelper;
import polman.controller.TrdtlPrestasiPemainFacade;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;

@Named("trdtlPrestasiPemainController")
@SessionScoped
public class TrdtlPrestasiPemainController implements Serializable {

    private TrdtlPrestasiPemain current;
    private DataModel items = null;
    @EJB
    private polman.controller.TrdtlPrestasiPemainFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;

    public TrdtlPrestasiPemainController() {
    }
    
    public List getAllMenu(){
//        List a = ejbFacade.getPrestasi();
        List a = ejbFacade.getPrestasi();
        List b = new ArrayList();
        if(a.size()>5)
        {
            for(int x=0;x<a.size();x++)
            {
                if(x==5)
                {
                    return b;
                }
                b.add(a.get(x));
            }
        }
        return b;
    }
    public List getAllPoin(){
        List a = ejbFacade.getPoin();
        return ejbFacade.getPoin();
    }
    public List getAllFoul(){
        List a = ejbFacade.getFoul();
        
        return ejbFacade.getFoul();
    }
    public TrdtlPrestasiPemain getSelected() {
        if (current == null) {
            current = new TrdtlPrestasiPemain();
            current.setTrdtlPrestasiPemainPK(new polman.model.TrdtlPrestasiPemainPK());
            selectedItemIndex = -1;
        }
        return current;
    }

    private TrdtlPrestasiPemainFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (TrdtlPrestasiPemain) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new TrdtlPrestasiPemain();
        current.setTrdtlPrestasiPemainPK(new polman.model.TrdtlPrestasiPemainPK());
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
        try {
            current.getTrdtlPrestasiPemainPK().setDtpIdcup(current.getTrCup().getCupId());
            current.getTrdtlPrestasiPemainPK().setDtpIdpem(current.getMsPeserta().getPesId());
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("TrdtlPrestasiPemainCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (TrdtlPrestasiPemain) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            current.getTrdtlPrestasiPemainPK().setDtpIdcup(current.getTrCup().getCupId());
            current.getTrdtlPrestasiPemainPK().setDtpIdpem(current.getMsPeserta().getPesId());
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("TrdtlPrestasiPemainUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (TrdtlPrestasiPemain) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("TrdtlPrestasiPemainDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public TrdtlPrestasiPemain getTrdtlPrestasiPemain(polman.model.TrdtlPrestasiPemainPK id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = TrdtlPrestasiPemain.class)
    public static class TrdtlPrestasiPemainControllerConverter implements Converter {

        private static final String SEPARATOR = "#";
        private static final String SEPARATOR_ESCAPED = "\\#";

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            TrdtlPrestasiPemainController controller = (TrdtlPrestasiPemainController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "trdtlPrestasiPemainController");
            return controller.getTrdtlPrestasiPemain(getKey(value));
        }

        polman.model.TrdtlPrestasiPemainPK getKey(String value) {
            polman.model.TrdtlPrestasiPemainPK key;
            String values[] = value.split(SEPARATOR_ESCAPED);
            key = new polman.model.TrdtlPrestasiPemainPK();
            key.setDtpIdcup(Integer.parseInt(values[0]));
            key.setDtpIdpem(Integer.parseInt(values[1]));
            return key;
        }

        String getStringKey(polman.model.TrdtlPrestasiPemainPK value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value.getDtpIdcup());
            sb.append(SEPARATOR);
            sb.append(value.getDtpIdpem());
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof TrdtlPrestasiPemain) {
                TrdtlPrestasiPemain o = (TrdtlPrestasiPemain) object;
                return getStringKey(o.getTrdtlPrestasiPemainPK());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + TrdtlPrestasiPemain.class.getName());
            }
        }

    }

}
