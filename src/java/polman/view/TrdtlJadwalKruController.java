package polman.view;

import polman.model.TrdtlJadwalKru;
import polman.view.util.JsfUtil;
import polman.view.util.PaginationHelper;
import polman.controller.TrdtlJadwalKruFacade;

import java.io.Serializable;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;

@Named("trdtlJadwalKruController")
@SessionScoped
public class TrdtlJadwalKruController implements Serializable {

    private TrdtlJadwalKru current;
    private DataModel items = null;
    @EJB
    private polman.controller.TrdtlJadwalKruFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;

    public TrdtlJadwalKruController() {
    }

    public TrdtlJadwalKru getSelected() {
        if (current == null) {
            current = new TrdtlJadwalKru();
            current.setTrdtlJadwalKruPK(new polman.model.TrdtlJadwalKruPK());
            selectedItemIndex = -1;
        }
        return current;
    }

    private TrdtlJadwalKruFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (TrdtlJadwalKru) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new TrdtlJadwalKru();
        current.setTrdtlJadwalKruPK(new polman.model.TrdtlJadwalKruPK());
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
        try {
            current.getTrdtlJadwalKruPK().setDtsIdjad(current.getTrJadwal().getJadId());
            current.getTrdtlJadwalKruPK().setDtsIdkru(current.getMsKru().getKruId());
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("TrdtlJadwalKruCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (TrdtlJadwalKru) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            current.getTrdtlJadwalKruPK().setDtsIdjad(current.getTrJadwal().getJadId());
            current.getTrdtlJadwalKruPK().setDtsIdkru(current.getMsKru().getKruId());
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("TrdtlJadwalKruUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (TrdtlJadwalKru) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("TrdtlJadwalKruDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public TrdtlJadwalKru getTrdtlJadwalKru(polman.model.TrdtlJadwalKruPK id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = TrdtlJadwalKru.class)
    public static class TrdtlJadwalKruControllerConverter implements Converter {

        private static final String SEPARATOR = "#";
        private static final String SEPARATOR_ESCAPED = "\\#";

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            TrdtlJadwalKruController controller = (TrdtlJadwalKruController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "trdtlJadwalKruController");
            return controller.getTrdtlJadwalKru(getKey(value));
        }

        polman.model.TrdtlJadwalKruPK getKey(String value) {
            polman.model.TrdtlJadwalKruPK key;
            String values[] = value.split(SEPARATOR_ESCAPED);
            key = new polman.model.TrdtlJadwalKruPK();
            key.setDtsIdkru(Integer.parseInt(values[0]));
            key.setDtsIdjad(Integer.parseInt(values[1]));
            return key;
        }

        String getStringKey(polman.model.TrdtlJadwalKruPK value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value.getDtsIdkru());
            sb.append(SEPARATOR);
            sb.append(value.getDtsIdjad());
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof TrdtlJadwalKru) {
                TrdtlJadwalKru o = (TrdtlJadwalKru) object;
                return getStringKey(o.getTrdtlJadwalKruPK());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + TrdtlJadwalKru.class.getName());
            }
        }

    }

}
