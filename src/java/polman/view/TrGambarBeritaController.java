package polman.view;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import polman.model.TrGambarBerita;
import polman.view.util.JsfUtil;
import polman.view.util.PaginationHelper;
import polman.controller.TrGambarBeritaFacade;

import java.io.Serializable;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.NavigationHandler;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.Part;
import polman.model.TrBerita;

@Named("trGambarBeritaController")
@SessionScoped
public class TrGambarBeritaController implements Serializable {

    private TrGambarBerita current;
    private DataModel items = null;
    @EJB
    private polman.controller.TrGambarBeritaFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;
    TrBerita idBeritaUpload;
    int idBeritaDetail;

    public int getIdBeritaDetail() {
        return idBeritaDetail;
    }

    public void setIdBeritaDetail(int idBeritaDetail) {
        this.idBeritaDetail = idBeritaDetail;
    }
    public List<TrGambarBerita> getAllGambar()
    {
        return ejbFacade.getGambarBerita(idBeritaDetail);
    }
    public TrGambarBeritaFacade getEjbFacade() {
        return ejbFacade;
    }

    public void setEjbFacade(TrGambarBeritaFacade ejbFacade) {
        this.ejbFacade = ejbFacade;
    }
    
    public TrBerita getIdBeritaUpload() {
        return idBeritaUpload;
    }

    public void setIdBeritaUpload(TrBerita idBeritaUpload) {
        this.idBeritaUpload = idBeritaUpload;
    }
    

    public TrGambarBeritaController() {
    }

    public TrGambarBerita getSelected() {
        if (current == null) {
            current = new TrGambarBerita();
            selectedItemIndex = -1;
        }
        return current;
    }

    private TrGambarBeritaFacade getFacade() {
        return ejbFacade;
    }
    private Part gambar;
    public Part getImage() {
        return gambar;
    }
    boolean gambarKosong;

    public boolean isGambarKosong() {
        return gambarKosong;
    }

    public void setGambarKosong(boolean gambarKosong) {
        this.gambarKosong = gambarKosong;
    }
    List x;
    List y = new ArrayList();
    public List getX() {
        return x;
    }

    public void setX(List x) {
        this.x = x;
    }
    public List getAllGambar(int id)
    {
        x= new ArrayList();
        List ab =  ejbFacade.getGambar(id);
            for(int c=0;c<ab.size();c++)
            {

                if(ab.size()==1)
                {
                    String cs=ejbFacade.getGambar(id).get(c).getGbrGambar();
                    x.add(cs);
                }
                String acs=ejbFacade.getGambar(id).get(c).getGbrGambar();
                x.add(acs);
            }
            
        return null;
    }
    public String selesaiGambar()
    {
        berhasilupload = false;
        datakosong=false;
        if(gambarUpload==0)
        {
            gambarKosong=true;
            return null;
        }
        gambarUpload=0;
        return "List";
    }
    public void setImage(Part gambar) {
        this.gambar = gambar;
    }
    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }
    public List getGambarDetail()
    {
        List a = ejbFacade.getGambarBerita(idBeritaDetail);
        return ejbFacade.getGambarBerita(idBeritaDetail);
    }

    public String prepareView() {
        current = (TrGambarBerita) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new TrGambarBerita();
        selectedItemIndex = -1;
        return "CreateGambar";
    }

    private String url;
    public String upload() {
        try {
            ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();  
            String path = servletContext.getRealPath("");
            InputStream in = gambar.getInputStream();
            setImage(gambar);
            File f = new File(path+"//resources//gambar//" + gambar.getSubmittedFileName());
            f.createNewFile();
//            url = f.toString();
            url = gambar.getSubmittedFileName();
            FileOutputStream out = new FileOutputStream(f);
            try (InputStream input = gambar.getInputStream()) {
                Files.copy(input, new File("C://Users//Candra//Documents//NetBeansProjects//GobakSodor//web//resources//gambar//" + gambar.getSubmittedFileName()).toPath());
            } catch (IOException e) {
                // Show faces message?
            }
            byte[] buffer = new byte[1024];
            int length;

            while ((length = in.read(buffer)) > 0) {
                out.write(buffer);
            }
            out.close();
            in.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return url;
    }
    int gambarUpload = 0;

    public int getGambarUpload() {
        return gambarUpload;
    }

    public void setGambarUpload(int gambarUpload) {
        this.gambarUpload = gambarUpload;
    }
    boolean datakosong =false;
    boolean berhasilupload = false;

    public boolean isBerhasilupload() {
        return berhasilupload;
    }

    public void setBerhasilupload(boolean berhasilupload) {
        this.berhasilupload = berhasilupload;
    }
    
    public boolean isDatakosong() {
        return datakosong;
    }

    public void setDatakosong(boolean datakosong) {
        this.datakosong = datakosong;
    }
    
    public String create() {
        try {
            if(current.getGbrCaption()==null||gambar.getSubmittedFileName()==null)
            {
                datakosong=true;
                return null;
            }
            datakosong=false;
            
            upload();
            current.setGbrGambar(url);
            current.setGbrBerita(idBeritaUpload);
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/baru/Bundle").getString("TrGambarBeritaCreated"));
            gambarUpload = gambarUpload+1;
            berhasilupload = true;
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/baru/Bundle").getString("PersistenceErrorOccured"));
            gambarKosong=false;
            datakosong=true;
            berhasilupload = false;
            return null;
        }
    }

    public String prepareEdit() {
        current = (TrGambarBerita) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/baru/Bundle").getString("TrGambarBeritaUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/baru/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (TrGambarBerita) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        getFacade().remove(current);
        recreatePagination();
        recreateModel();
        return "Edit";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/baru/Bundle").getString("TrGambarBeritaDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/baru/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public TrGambarBerita getTrGambarBerita(java.lang.Integer id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = TrGambarBerita.class)
    public static class TrGambarBeritaControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            TrGambarBeritaController controller = (TrGambarBeritaController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "trGambarBeritaController");
            return controller.getTrGambarBerita(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof TrGambarBerita) {
                TrGambarBerita o = (TrGambarBerita) object;
                return getStringKey(o.getGbrId());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + TrGambarBerita.class.getName());
            }
        }

    }

}
