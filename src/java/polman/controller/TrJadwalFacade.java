/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polman.controller;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import polman.model.TrJadwal;

/**
 *
 * @author Jihad044
 */
@Stateless
public class TrJadwalFacade extends AbstractFacade<TrJadwal> {

    @PersistenceContext(unitName = "GobakSodorPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TrJadwalFacade() {
        super(TrJadwal.class);
    }
    
}
