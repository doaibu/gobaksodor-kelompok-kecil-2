/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polman.controller;

import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import polman.model.TrBerita;

/**
 *
 * @author Candra
 */
@Stateless
public class TrBeritaFacade extends AbstractFacade<TrBerita> {

    @PersistenceContext(unitName = "GobakSodorPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TrBeritaFacade() {
        super(TrBerita.class);
    }
    public List getAllBerita()
    {
        return em.createNamedQuery("TrBerita.findHL",TrBerita.class)
                .getResultList();
    }
    public List getAllPopuler()
    {
        return em.createNamedQuery("TrBerita.findPL",TrBerita.class)
                .getResultList();
    }
    public List getBeritaDetail(int id)
    {
        return em.createNamedQuery("TrBerita.findByBerId",TrBerita.class)
                .setParameter("id", id)
                .getResultList();
    }
    public List getBeritaKomentar()
    {
        return em.createNamedQuery("TrBerita.findKL",TrBerita.class)
                .getResultList();
    }
    public List getBeritaLast(Date tgl)
    {
        return em.createNamedQuery("TrBerita.findUP",TrBerita.class)
                .setParameter("tgl", tgl)
                .getResultList();
    }
}
