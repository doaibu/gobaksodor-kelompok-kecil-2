/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polman.controller;

import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import polman.model.TrKomentar;
import polman.controller.AbstractFacade;
import polman.model.TrBerita;

/**
 *
 * @author Pebry Harnelfi Mirza
 */
@Stateless
public class TrKomentarFacade extends AbstractFacade<TrKomentar> {

    @PersistenceContext(unitName = "GobakSodorPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TrKomentarFacade() {
        super(TrKomentar.class);
    }
    public List getKomentar(int parent)
    {
        return em.createNamedQuery("TrKomentar.findKomentar",TrKomentar.class)
                .setParameter("parent", parent)
                .getResultList();
    }
    public List getKomentarSatu(int parent)
    {
        return em.createNamedQuery("TrKomentar.findKomentarSatu",TrKomentar.class)
                .setParameter("parent", parent)
                .getResultList();
    }
    public List getParent(int parent)
    {
        return em.createNamedQuery("TrKomentar.findParent",TrKomentar.class)
                .setParameter("parent", parent)
                .getResultList();
    }
    
}
