/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polman.controller;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import polman.model.MsKru;

/**
 *
 * @author Jihad044
 */
@Stateless
public class MsKruFacade extends AbstractFacade<MsKru> {

    @PersistenceContext(unitName = "GobakSodorPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MsKruFacade() {
        super(MsKru.class);
    }
    
}
