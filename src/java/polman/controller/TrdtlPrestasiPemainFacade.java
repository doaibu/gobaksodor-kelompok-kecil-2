/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polman.controller;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import polman.model.TrdtlPrestasiPemain;

/**
 *
 * @author Jihad044
 */
@Stateless
public class TrdtlPrestasiPemainFacade extends AbstractFacade<TrdtlPrestasiPemain> {

    @PersistenceContext(unitName = "GobakSodorPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TrdtlPrestasiPemainFacade() {
        super(TrdtlPrestasiPemain.class);
    }
    public List<TrdtlPrestasiPemain> getPrestasi(){
        return em.createNamedQuery("TrdtlPrestasiPemain.findAll", TrdtlPrestasiPemain.class)
                .getResultList();
    }
    public List<TrdtlPrestasiPemain> getPoin(){
        return em.createNamedQuery("TrdtlPrestasiPemain.findPlus", TrdtlPrestasiPemain.class)
                .getResultList();
    }
    public List<TrdtlPrestasiPemain> getFoul(){
        return em.createNamedQuery("TrdtlPrestasiPemain.findFoul", TrdtlPrestasiPemain.class)
                .getResultList();
    }
}
