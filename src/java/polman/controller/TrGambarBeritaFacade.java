/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polman.controller;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import polman.model.TrGambarBerita;
import polman.model.TrdtlPrestasiPemain;

/**
 *
 * @author Candra
 */
@Stateless
public class TrGambarBeritaFacade extends AbstractFacade<TrGambarBerita> {

    @PersistenceContext(unitName = "GobakSodorPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TrGambarBeritaFacade() {
        super(TrGambarBerita.class);
    }
    public List<TrGambarBerita> getGambar(int id){
        return em.createNamedQuery("TrGambarBerita.findByGbrId", TrGambarBerita.class)
                .setParameter("gbrId",id )
                .getResultList();
    }
    public List getGambarBerita(int idBerita){
        return em.createNamedQuery("TrGambarBerita.findByIdBerita", TrGambarBerita.class)
                .setParameter("id",idBerita )
                .getResultList();
    }
}
