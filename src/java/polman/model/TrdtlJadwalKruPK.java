/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polman.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Candra
 */
@Embeddable
public class TrdtlJadwalKruPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "dts_idkru")
    private int dtsIdkru;
    @Basic(optional = false)
    @NotNull
    @Column(name = "dts_idjad")
    private int dtsIdjad;

    public TrdtlJadwalKruPK() {
    }

    public TrdtlJadwalKruPK(int dtsIdkru, int dtsIdjad) {
        this.dtsIdkru = dtsIdkru;
        this.dtsIdjad = dtsIdjad;
    }

    public int getDtsIdkru() {
        return dtsIdkru;
    }

    public void setDtsIdkru(int dtsIdkru) {
        this.dtsIdkru = dtsIdkru;
    }

    public int getDtsIdjad() {
        return dtsIdjad;
    }

    public void setDtsIdjad(int dtsIdjad) {
        this.dtsIdjad = dtsIdjad;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) dtsIdkru;
        hash += (int) dtsIdjad;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TrdtlJadwalKruPK)) {
            return false;
        }
        TrdtlJadwalKruPK other = (TrdtlJadwalKruPK) object;
        if (this.dtsIdkru != other.dtsIdkru) {
            return false;
        }
        if (this.dtsIdjad != other.dtsIdjad) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "polman.model.TrdtlJadwalKruPK[ dtsIdkru=" + dtsIdkru + ", dtsIdjad=" + dtsIdjad + " ]";
    }
    
}
