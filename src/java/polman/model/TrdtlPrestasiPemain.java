/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polman.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Candra
 */
@Entity
@Table(name = "tr_dtlPrestasiPemain")
@NamedQueries({
    @NamedQuery(name = "TrdtlPrestasiPemain.findAll", query = "SELECT CONCAT ( pes.pesNamaDepan, ' ', pes.pesNamaBelakang ) as nama, t.dtpPoinMinus as tangkap,ms.timNama as tim  FROM TrdtlPrestasiPemain t,MsPeserta pes,MsTim ms WHERE pes.pesId = t.msPeserta.pesId and ms.timId = pes.pesIdTim.timId and t.dtpPoinMinus > '0' ORDER BY t.dtpPoinMinus DESC")
        ,@NamedQuery(name = "TrdtlPrestasiPemain.findPlus", query = "SELECT CONCAT ( pes.pesNamaDepan, ' ', pes.pesNamaBelakang ) as nama, t.dtpPoinPlus as lolos,ms.timNama as tim  FROM TrdtlPrestasiPemain t,MsPeserta pes,MsTim ms WHERE pes.pesId = t.msPeserta.pesId and ms.timId = pes.pesIdTim.timId and t.dtpPoinPlus > '0' ORDER BY t.dtpPoinPlus DESC")
        ,@NamedQuery(name = "TrdtlPrestasiPemain.findFoul", query = "SELECT CONCAT ( pes.pesNamaDepan, ' ', pes.pesNamaBelakang ) as nama, t.dtpJumlahPelanggaran as foul,ms.timNama as tim  FROM TrdtlPrestasiPemain t,MsPeserta pes,MsTim ms WHERE pes.pesId = t.msPeserta.pesId and ms.timId = pes.pesIdTim.timId and t.dtpJumlahPelanggaran > '0' ORDER BY t.dtpJumlahPelanggaran DESC") 
    , @NamedQuery(name = "TrdtlPrestasiPemain.findByDtpIdcup", query = "SELECT t FROM TrdtlPrestasiPemain t WHERE t.trdtlPrestasiPemainPK.dtpIdcup = :dtpIdcup")
    , @NamedQuery(name = "TrdtlPrestasiPemain.findByDtpIdpem", query = "SELECT t FROM TrdtlPrestasiPemain t WHERE t.trdtlPrestasiPemainPK.dtpIdpem = :dtpIdpem")
    , @NamedQuery(name = "TrdtlPrestasiPemain.findByDtpPoinPlus", query = "SELECT t FROM TrdtlPrestasiPemain t WHERE t.dtpPoinPlus = :dtpPoinPlus")
    , @NamedQuery(name = "TrdtlPrestasiPemain.findByDtpPoinMinus", query = "SELECT t FROM TrdtlPrestasiPemain t WHERE t.dtpPoinMinus = :dtpPoinMinus")
    , @NamedQuery(name = "TrdtlPrestasiPemain.findByDtpJumlahPelanggaran", query = "SELECT t FROM TrdtlPrestasiPemain t WHERE t.dtpJumlahPelanggaran = :dtpJumlahPelanggaran")})
public class TrdtlPrestasiPemain implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected TrdtlPrestasiPemainPK trdtlPrestasiPemainPK;
    @Column(name = "dtp_poin_plus")
    private Integer dtpPoinPlus;
    @Column(name = "dtp_poin_minus")
    private Integer dtpPoinMinus;
    @Column(name = "dtp_jumlah_pelanggaran")
    private Integer dtpJumlahPelanggaran;
    @JoinColumn(name = "dtp_idpem", referencedColumnName = "pes_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private MsPeserta msPeserta;
    @JoinColumn(name = "dtp_idcup", referencedColumnName = "cup_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private TrCup trCup;

    public TrdtlPrestasiPemain() {
    }

    public TrdtlPrestasiPemain(TrdtlPrestasiPemainPK trdtlPrestasiPemainPK) {
        this.trdtlPrestasiPemainPK = trdtlPrestasiPemainPK;
    }

    public TrdtlPrestasiPemain(int dtpIdcup, int dtpIdpem) {
        this.trdtlPrestasiPemainPK = new TrdtlPrestasiPemainPK(dtpIdcup, dtpIdpem);
    }

    public TrdtlPrestasiPemainPK getTrdtlPrestasiPemainPK() {
        return trdtlPrestasiPemainPK;
    }

    public void setTrdtlPrestasiPemainPK(TrdtlPrestasiPemainPK trdtlPrestasiPemainPK) {
        this.trdtlPrestasiPemainPK = trdtlPrestasiPemainPK;
    }

    public Integer getDtpPoinPlus() {
        return dtpPoinPlus;
    }

    public void setDtpPoinPlus(Integer dtpPoinPlus) {
        this.dtpPoinPlus = dtpPoinPlus;
    }

    public Integer getDtpPoinMinus() {
        return dtpPoinMinus;
    }

    public void setDtpPoinMinus(Integer dtpPoinMinus) {
        this.dtpPoinMinus = dtpPoinMinus;
    }

    public Integer getDtpJumlahPelanggaran() {
        return dtpJumlahPelanggaran;
    }

    public void setDtpJumlahPelanggaran(Integer dtpJumlahPelanggaran) {
        this.dtpJumlahPelanggaran = dtpJumlahPelanggaran;
    }

    public MsPeserta getMsPeserta() {
        return msPeserta;
    }

    public void setMsPeserta(MsPeserta msPeserta) {
        this.msPeserta = msPeserta;
    }

    public TrCup getTrCup() {
        return trCup;
    }

    public void setTrCup(TrCup trCup) {
        this.trCup = trCup;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (trdtlPrestasiPemainPK != null ? trdtlPrestasiPemainPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TrdtlPrestasiPemain)) {
            return false;
        }
        TrdtlPrestasiPemain other = (TrdtlPrestasiPemain) object;
        if ((this.trdtlPrestasiPemainPK == null && other.trdtlPrestasiPemainPK != null) || (this.trdtlPrestasiPemainPK != null && !this.trdtlPrestasiPemainPK.equals(other.trdtlPrestasiPemainPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "polman.model.TrdtlPrestasiPemain[ trdtlPrestasiPemainPK=" + trdtlPrestasiPemainPK + " ]";
    }
    
}
