/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polman.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Candra
 */
@Embeddable
public class TrdtlPertandinganPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "dts_idjad")
    private int dtsIdjad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "dts_idpem")
    private int dtsIdpem;

    public TrdtlPertandinganPK() {
    }

    public TrdtlPertandinganPK(int dtsIdjad, int dtsIdpem) {
        this.dtsIdjad = dtsIdjad;
        this.dtsIdpem = dtsIdpem;
    }

    public int getDtsIdjad() {
        return dtsIdjad;
    }

    public void setDtsIdjad(int dtsIdjad) {
        this.dtsIdjad = dtsIdjad;
    }

    public int getDtsIdpem() {
        return dtsIdpem;
    }

    public void setDtsIdpem(int dtsIdpem) {
        this.dtsIdpem = dtsIdpem;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) dtsIdjad;
        hash += (int) dtsIdpem;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TrdtlPertandinganPK)) {
            return false;
        }
        TrdtlPertandinganPK other = (TrdtlPertandinganPK) object;
        if (this.dtsIdjad != other.dtsIdjad) {
            return false;
        }
        if (this.dtsIdpem != other.dtsIdpem) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "polman.model.TrdtlPertandinganPK[ dtsIdjad=" + dtsIdjad + ", dtsIdpem=" + dtsIdpem + " ]";
    }
    
}
