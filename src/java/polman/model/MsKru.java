/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polman.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Candra
 */
@Entity
@Table(name = "ms_kru")
@NamedQueries({
    @NamedQuery(name = "MsKru.findAll", query = "SELECT m FROM MsKru m")
    , @NamedQuery(name = "MsKru.findByKruId", query = "SELECT m FROM MsKru m WHERE m.kruId = :kruId")
    , @NamedQuery(name = "MsKru.findByKruNamaDepan", query = "SELECT m FROM MsKru m WHERE m.kruNamaDepan = :kruNamaDepan")
    , @NamedQuery(name = "MsKru.findByKruNamaBelakang", query = "SELECT m FROM MsKru m WHERE m.kruNamaBelakang = :kruNamaBelakang")
    , @NamedQuery(name = "MsKru.findByKruTglLahir", query = "SELECT m FROM MsKru m WHERE m.kruTglLahir = :kruTglLahir")
    , @NamedQuery(name = "MsKru.findByKruNoTlp", query = "SELECT m FROM MsKru m WHERE m.kruNoTlp = :kruNoTlp")
    , @NamedQuery(name = "MsKru.findByKruFoto", query = "SELECT m FROM MsKru m WHERE m.kruFoto = :kruFoto")
    , @NamedQuery(name = "MsKru.findByKruCreatedby", query = "SELECT m FROM MsKru m WHERE m.kruCreatedby = :kruCreatedby")
    , @NamedQuery(name = "MsKru.findByKruModivedby", query = "SELECT m FROM MsKru m WHERE m.kruModivedby = :kruModivedby")
    , @NamedQuery(name = "MsKru.findByKruCreateddate", query = "SELECT m FROM MsKru m WHERE m.kruCreateddate = :kruCreateddate")
    , @NamedQuery(name = "MsKru.findByKruModiveddate", query = "SELECT m FROM MsKru m WHERE m.kruModiveddate = :kruModiveddate")})
public class MsKru implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "kru_id")
    private Integer kruId;
    @Size(max = 50)
    @Column(name = "kru_nama_depan")
    private String kruNamaDepan;
    @Size(max = 50)
    @Column(name = "kru_nama_belakang")
    private String kruNamaBelakang;
    @Column(name = "kru_tgl_lahir")
    @Temporal(TemporalType.DATE)
    private Date kruTglLahir;
    @Size(max = 50)
    @Column(name = "kru_no_tlp")
    private String kruNoTlp;
    @Size(max = 150)
    @Column(name = "kru_foto")
    private String kruFoto;
    @Size(max = 50)
    @Column(name = "kru_createdby")
    private String kruCreatedby;
    @Size(max = 50)
    @Column(name = "kru_modivedby")
    private String kruModivedby;
    @Column(name = "kru_createddate")
    @Temporal(TemporalType.DATE)
    private Date kruCreateddate;
    @Column(name = "kru_modiveddate")
    @Temporal(TemporalType.DATE)
    private Date kruModiveddate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "msKru")
    private Collection<TrdtlJadwalKru> trdtlJadwalKruCollection;

    public MsKru() {
    }

    public MsKru(Integer kruId) {
        this.kruId = kruId;
    }

    public Integer getKruId() {
        return kruId;
    }

    public void setKruId(Integer kruId) {
        this.kruId = kruId;
    }

    public String getKruNamaDepan() {
        return kruNamaDepan;
    }

    public void setKruNamaDepan(String kruNamaDepan) {
        this.kruNamaDepan = kruNamaDepan;
    }

    public String getKruNamaBelakang() {
        return kruNamaBelakang;
    }

    public void setKruNamaBelakang(String kruNamaBelakang) {
        this.kruNamaBelakang = kruNamaBelakang;
    }

    public Date getKruTglLahir() {
        return kruTglLahir;
    }

    public void setKruTglLahir(Date kruTglLahir) {
        this.kruTglLahir = kruTglLahir;
    }

    public String getKruNoTlp() {
        return kruNoTlp;
    }

    public void setKruNoTlp(String kruNoTlp) {
        this.kruNoTlp = kruNoTlp;
    }

    public String getKruFoto() {
        return kruFoto;
    }

    public void setKruFoto(String kruFoto) {
        this.kruFoto = kruFoto;
    }

    public String getKruCreatedby() {
        return kruCreatedby;
    }

    public void setKruCreatedby(String kruCreatedby) {
        this.kruCreatedby = kruCreatedby;
    }

    public String getKruModivedby() {
        return kruModivedby;
    }

    public void setKruModivedby(String kruModivedby) {
        this.kruModivedby = kruModivedby;
    }

    public Date getKruCreateddate() {
        return kruCreateddate;
    }

    public void setKruCreateddate(Date kruCreateddate) {
        this.kruCreateddate = kruCreateddate;
    }

    public Date getKruModiveddate() {
        return kruModiveddate;
    }

    public void setKruModiveddate(Date kruModiveddate) {
        this.kruModiveddate = kruModiveddate;
    }

    public Collection<TrdtlJadwalKru> getTrdtlJadwalKruCollection() {
        return trdtlJadwalKruCollection;
    }

    public void setTrdtlJadwalKruCollection(Collection<TrdtlJadwalKru> trdtlJadwalKruCollection) {
        this.trdtlJadwalKruCollection = trdtlJadwalKruCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (kruId != null ? kruId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MsKru)) {
            return false;
        }
        MsKru other = (MsKru) object;
        if ((this.kruId == null && other.kruId != null) || (this.kruId != null && !this.kruId.equals(other.kruId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "polman.model.MsKru[ kruId=" + kruId + " ]";
    }
    
}
