/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polman.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author Candra
 */
@Entity
@Table(name = "tr_dtlJadwalKru")
@NamedQueries({
    @NamedQuery(name = "TrdtlJadwalKru.findAll", query = "SELECT t FROM TrdtlJadwalKru t")
    , @NamedQuery(name = "TrdtlJadwalKru.findByDtsIdkru", query = "SELECT t FROM TrdtlJadwalKru t WHERE t.trdtlJadwalKruPK.dtsIdkru = :dtsIdkru")
    , @NamedQuery(name = "TrdtlJadwalKru.findByDtsIdjad", query = "SELECT t FROM TrdtlJadwalKru t WHERE t.trdtlJadwalKruPK.dtsIdjad = :dtsIdjad")
    , @NamedQuery(name = "TrdtlJadwalKru.findByDtsRole", query = "SELECT t FROM TrdtlJadwalKru t WHERE t.dtsRole = :dtsRole")})
public class TrdtlJadwalKru implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected TrdtlJadwalKruPK trdtlJadwalKruPK;
    @Size(max = 50)
    @Column(name = "dts_role")
    private String dtsRole;
    @JoinColumn(name = "dts_idkru", referencedColumnName = "kru_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private MsKru msKru;
    @JoinColumn(name = "dts_idjad", referencedColumnName = "jad_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private TrJadwal trJadwal;

    public TrdtlJadwalKru() {
    }

    public TrdtlJadwalKru(TrdtlJadwalKruPK trdtlJadwalKruPK) {
        this.trdtlJadwalKruPK = trdtlJadwalKruPK;
    }

    public TrdtlJadwalKru(int dtsIdkru, int dtsIdjad) {
        this.trdtlJadwalKruPK = new TrdtlJadwalKruPK(dtsIdkru, dtsIdjad);
    }

    public TrdtlJadwalKruPK getTrdtlJadwalKruPK() {
        return trdtlJadwalKruPK;
    }

    public void setTrdtlJadwalKruPK(TrdtlJadwalKruPK trdtlJadwalKruPK) {
        this.trdtlJadwalKruPK = trdtlJadwalKruPK;
    }

    public String getDtsRole() {
        return dtsRole;
    }

    public void setDtsRole(String dtsRole) {
        this.dtsRole = dtsRole;
    }

    public MsKru getMsKru() {
        return msKru;
    }

    public void setMsKru(MsKru msKru) {
        this.msKru = msKru;
    }

    public TrJadwal getTrJadwal() {
        return trJadwal;
    }

    public void setTrJadwal(TrJadwal trJadwal) {
        this.trJadwal = trJadwal;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (trdtlJadwalKruPK != null ? trdtlJadwalKruPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TrdtlJadwalKru)) {
            return false;
        }
        TrdtlJadwalKru other = (TrdtlJadwalKru) object;
        if ((this.trdtlJadwalKruPK == null && other.trdtlJadwalKruPK != null) || (this.trdtlJadwalKruPK != null && !this.trdtlJadwalKruPK.equals(other.trdtlJadwalKruPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "polman.model.TrdtlJadwalKru[ trdtlJadwalKruPK=" + trdtlJadwalKruPK + " ]";
    }
    
}
