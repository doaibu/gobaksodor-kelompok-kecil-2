/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polman.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Candra
 */
@Entity
@Table(name = "ms_Lapangan")
@NamedQueries({
    @NamedQuery(name = "MsLapangan.findAll", query = "SELECT m FROM MsLapangan m")
    , @NamedQuery(name = "MsLapangan.findByLapId", query = "SELECT m FROM MsLapangan m WHERE m.lapId = :lapId")
    , @NamedQuery(name = "MsLapangan.findByLapNama", query = "SELECT m FROM MsLapangan m WHERE m.lapNama = :lapNama")
    , @NamedQuery(name = "MsLapangan.findByLapAlamat", query = "SELECT m FROM MsLapangan m WHERE m.lapAlamat = :lapAlamat")
    , @NamedQuery(name = "MsLapangan.findByLapFoto", query = "SELECT m FROM MsLapangan m WHERE m.lapFoto = :lapFoto")
    , @NamedQuery(name = "MsLapangan.findByLapCreatedby", query = "SELECT m FROM MsLapangan m WHERE m.lapCreatedby = :lapCreatedby")
    , @NamedQuery(name = "MsLapangan.findByLapModivedby", query = "SELECT m FROM MsLapangan m WHERE m.lapModivedby = :lapModivedby")
    , @NamedQuery(name = "MsLapangan.findByLapCreateddate", query = "SELECT m FROM MsLapangan m WHERE m.lapCreateddate = :lapCreateddate")
    , @NamedQuery(name = "MsLapangan.findByLapModivedate", query = "SELECT m FROM MsLapangan m WHERE m.lapModivedate = :lapModivedate")})
public class MsLapangan implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "lap_id")
    private Integer lapId;
    @Size(max = 50)
    @Column(name = "lap_nama")
    private String lapNama;
    @Size(max = 150)
    @Column(name = "lap_alamat")
    private String lapAlamat;
    @Size(max = 150)
    @Column(name = "lap_foto")
    private String lapFoto;
    @Size(max = 50)
    @Column(name = "lap_createdby")
    private String lapCreatedby;
    @Size(max = 50)
    @Column(name = "lap_modivedby")
    private String lapModivedby;
    @Column(name = "lap_createddate")
    @Temporal(TemporalType.DATE)
    private Date lapCreateddate;
    @Column(name = "lap_modivedate")
    @Temporal(TemporalType.DATE)
    private Date lapModivedate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "jadLapangan")
    private Collection<TrJadwal> trJadwalCollection;

    public MsLapangan() {
    }

    public MsLapangan(Integer lapId) {
        this.lapId = lapId;
    }

    public Integer getLapId() {
        return lapId;
    }

    public void setLapId(Integer lapId) {
        this.lapId = lapId;
    }

    public String getLapNama() {
        return lapNama;
    }

    public void setLapNama(String lapNama) {
        this.lapNama = lapNama;
    }

    public String getLapAlamat() {
        return lapAlamat;
    }

    public void setLapAlamat(String lapAlamat) {
        this.lapAlamat = lapAlamat;
    }

    public String getLapFoto() {
        return lapFoto;
    }

    public void setLapFoto(String lapFoto) {
        this.lapFoto = lapFoto;
    }

    public String getLapCreatedby() {
        return lapCreatedby;
    }

    public void setLapCreatedby(String lapCreatedby) {
        this.lapCreatedby = lapCreatedby;
    }

    public String getLapModivedby() {
        return lapModivedby;
    }

    public void setLapModivedby(String lapModivedby) {
        this.lapModivedby = lapModivedby;
    }

    public Date getLapCreateddate() {
        return lapCreateddate;
    }

    public void setLapCreateddate(Date lapCreateddate) {
        this.lapCreateddate = lapCreateddate;
    }

    public Date getLapModivedate() {
        return lapModivedate;
    }

    public void setLapModivedate(Date lapModivedate) {
        this.lapModivedate = lapModivedate;
    }

    public Collection<TrJadwal> getTrJadwalCollection() {
        return trJadwalCollection;
    }

    public void setTrJadwalCollection(Collection<TrJadwal> trJadwalCollection) {
        this.trJadwalCollection = trJadwalCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (lapId != null ? lapId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MsLapangan)) {
            return false;
        }
        MsLapangan other = (MsLapangan) object;
        if ((this.lapId == null && other.lapId != null) || (this.lapId != null && !this.lapId.equals(other.lapId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "polman.model.MsLapangan[ lapId=" + lapId + " ]";
    }
    
}
