/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polman.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Candra
 */
@Entity
@Table(name = "ms_Peserta")
@NamedQueries({
    @NamedQuery(name = "MsPeserta.findAll", query = "SELECT m FROM MsPeserta m")
    , @NamedQuery(name = "MsPeserta.findByPesId", query = "SELECT m FROM MsPeserta m WHERE m.pesId = :pesId")
    , @NamedQuery(name = "MsPeserta.findByPesNim", query = "SELECT m FROM MsPeserta m WHERE m.pesNim = :pesNim")
    , @NamedQuery(name = "MsPeserta.findByPesNamaDepan", query = "SELECT m FROM MsPeserta m WHERE m.pesNamaDepan = :pesNamaDepan")
    , @NamedQuery(name = "MsPeserta.findByPesNamaBelakang", query = "SELECT m FROM MsPeserta m WHERE m.pesNamaBelakang = :pesNamaBelakang")
    , @NamedQuery(name = "MsPeserta.findByPesNoTelp", query = "SELECT m FROM MsPeserta m WHERE m.pesNoTelp = :pesNoTelp")
    , @NamedQuery(name = "MsPeserta.findByPesTglLahir", query = "SELECT m FROM MsPeserta m WHERE m.pesTglLahir = :pesTglLahir")
    , @NamedQuery(name = "MsPeserta.findByPesRole", query = "SELECT m FROM MsPeserta m WHERE m.pesRole = :pesRole")
    , @NamedQuery(name = "MsPeserta.findByPesFoto", query = "SELECT m FROM MsPeserta m WHERE m.pesFoto = :pesFoto")
    , @NamedQuery(name = "MsPeserta.findByPesCreatedby", query = "SELECT m FROM MsPeserta m WHERE m.pesCreatedby = :pesCreatedby")
    , @NamedQuery(name = "MsPeserta.findByPesModivedby", query = "SELECT m FROM MsPeserta m WHERE m.pesModivedby = :pesModivedby")
    , @NamedQuery(name = "MsPeserta.findByPesCreateddate", query = "SELECT m FROM MsPeserta m WHERE m.pesCreateddate = :pesCreateddate")
    , @NamedQuery(name = "MsPeserta.findByPesModiveddate", query = "SELECT m FROM MsPeserta m WHERE m.pesModiveddate = :pesModiveddate")})
public class MsPeserta implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "pes_id")
    private Integer pesId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "pes_nim")
    private String pesNim;
    @Size(max = 50)
    @Column(name = "pes_nama_depan")
    private String pesNamaDepan;
    @Size(max = 50)
    @Column(name = "pes_nama_belakang")
    private String pesNamaBelakang;
    @Size(max = 20)
    @Column(name = "pes_no_telp")
    private String pesNoTelp;
    @Column(name = "pes_tgl_lahir")
    @Temporal(TemporalType.DATE)
    private Date pesTglLahir;
    @Size(max = 50)
    @Column(name = "pes_role")
    private String pesRole;
    @Size(max = 150)
    @Column(name = "pes_foto")
    private String pesFoto;
    @Size(max = 50)
    @Column(name = "pes_createdby")
    private String pesCreatedby;
    @Size(max = 50)
    @Column(name = "pes_modivedby")
    private String pesModivedby;
    @Column(name = "pes_createddate")
    @Temporal(TemporalType.DATE)
    private Date pesCreateddate;
    @Column(name = "pes_modiveddate")
    @Temporal(TemporalType.DATE)
    private Date pesModiveddate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "msPeserta")
    private Collection<TrdtlPertandingan> trdtlPertandinganCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "msPeserta")
    private Collection<TrdtlPrestasiPemain> trdtlPrestasiPemainCollection;
    @JoinColumn(name = "pes_id_tim", referencedColumnName = "tim_id")
    @ManyToOne
    private MsTim pesIdTim;

    public MsPeserta() {
    }

    public MsPeserta(Integer pesId) {
        this.pesId = pesId;
    }

    public MsPeserta(Integer pesId, String pesNim) {
        this.pesId = pesId;
        this.pesNim = pesNim;
    }

    public Integer getPesId() {
        return pesId;
    }

    public void setPesId(Integer pesId) {
        this.pesId = pesId;
    }

    public String getPesNim() {
        return pesNim;
    }

    public void setPesNim(String pesNim) {
        this.pesNim = pesNim;
    }

    public String getPesNamaDepan() {
        return pesNamaDepan;
    }

    public void setPesNamaDepan(String pesNamaDepan) {
        this.pesNamaDepan = pesNamaDepan;
    }

    public String getPesNamaBelakang() {
        return pesNamaBelakang;
    }

    public void setPesNamaBelakang(String pesNamaBelakang) {
        this.pesNamaBelakang = pesNamaBelakang;
    }

    public String getPesNoTelp() {
        return pesNoTelp;
    }

    public void setPesNoTelp(String pesNoTelp) {
        this.pesNoTelp = pesNoTelp;
    }

    public Date getPesTglLahir() {
        return pesTglLahir;
    }

    public void setPesTglLahir(Date pesTglLahir) {
        this.pesTglLahir = pesTglLahir;
    }

    public String getPesRole() {
        return pesRole;
    }

    public void setPesRole(String pesRole) {
        this.pesRole = pesRole;
    }

    public String getPesFoto() {
        return pesFoto;
    }

    public void setPesFoto(String pesFoto) {
        this.pesFoto = pesFoto;
    }

    public String getPesCreatedby() {
        return pesCreatedby;
    }

    public void setPesCreatedby(String pesCreatedby) {
        this.pesCreatedby = pesCreatedby;
    }

    public String getPesModivedby() {
        return pesModivedby;
    }

    public void setPesModivedby(String pesModivedby) {
        this.pesModivedby = pesModivedby;
    }

    public Date getPesCreateddate() {
        return pesCreateddate;
    }

    public void setPesCreateddate(Date pesCreateddate) {
        this.pesCreateddate = pesCreateddate;
    }

    public Date getPesModiveddate() {
        return pesModiveddate;
    }

    public void setPesModiveddate(Date pesModiveddate) {
        this.pesModiveddate = pesModiveddate;
    }

    public Collection<TrdtlPertandingan> getTrdtlPertandinganCollection() {
        return trdtlPertandinganCollection;
    }

    public void setTrdtlPertandinganCollection(Collection<TrdtlPertandingan> trdtlPertandinganCollection) {
        this.trdtlPertandinganCollection = trdtlPertandinganCollection;
    }

    public Collection<TrdtlPrestasiPemain> getTrdtlPrestasiPemainCollection() {
        return trdtlPrestasiPemainCollection;
    }

    public void setTrdtlPrestasiPemainCollection(Collection<TrdtlPrestasiPemain> trdtlPrestasiPemainCollection) {
        this.trdtlPrestasiPemainCollection = trdtlPrestasiPemainCollection;
    }

    public MsTim getPesIdTim() {
        return pesIdTim;
    }

    public void setPesIdTim(MsTim pesIdTim) {
        this.pesIdTim = pesIdTim;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pesId != null ? pesId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MsPeserta)) {
            return false;
        }
        MsPeserta other = (MsPeserta) object;
        if ((this.pesId == null && other.pesId != null) || (this.pesId != null && !this.pesId.equals(other.pesId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "polman.model.MsPeserta[ pesId=" + pesId + " ]";
    }
    
}
