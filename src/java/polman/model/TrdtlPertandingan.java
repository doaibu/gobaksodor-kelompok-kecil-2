/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polman.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Candra
 */
@Entity
@Table(name = "tr_dtlPertandingan")
@NamedQueries({
    @NamedQuery(name = "TrdtlPertandingan.findAll", query = "SELECT t FROM TrdtlPertandingan t")
    , @NamedQuery(name = "TrdtlPertandingan.findByDtsIdjad", query = "SELECT t FROM TrdtlPertandingan t WHERE t.trdtlPertandinganPK.dtsIdjad = :dtsIdjad")
    , @NamedQuery(name = "TrdtlPertandingan.findByDtsIdpem", query = "SELECT t FROM TrdtlPertandingan t WHERE t.trdtlPertandinganPK.dtsIdpem = :dtsIdpem")
    , @NamedQuery(name = "TrdtlPertandingan.findByMenit", query = "SELECT t FROM TrdtlPertandingan t WHERE t.menit = :menit")
    , @NamedQuery(name = "TrdtlPertandingan.findByDtsPelanggaran", query = "SELECT t FROM TrdtlPertandingan t WHERE t.dtsPelanggaran = :dtsPelanggaran")
    , @NamedQuery(name = "TrdtlPertandingan.findByDtsTangkap", query = "SELECT t FROM TrdtlPertandingan t WHERE t.dtsTangkap = :dtsTangkap")
    , @NamedQuery(name = "TrdtlPertandingan.findByDtsLolos", query = "SELECT t FROM TrdtlPertandingan t WHERE t.dtsLolos = :dtsLolos")})
public class TrdtlPertandingan implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected TrdtlPertandinganPK trdtlPertandinganPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "menit")
    @Temporal(TemporalType.TIME)
    private Date menit;
    @Column(name = "dts_pelanggaran")
    private Integer dtsPelanggaran;
    @Column(name = "dts_tangkap")
    private Integer dtsTangkap;
    @Column(name = "dts_lolos")
    private Integer dtsLolos;
    @JoinColumn(name = "dts_idpem", referencedColumnName = "pes_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private MsPeserta msPeserta;
    @JoinColumn(name = "dts_idjad", referencedColumnName = "jad_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private TrJadwal trJadwal;

    public TrdtlPertandingan() {
    }

    public TrdtlPertandingan(TrdtlPertandinganPK trdtlPertandinganPK) {
        this.trdtlPertandinganPK = trdtlPertandinganPK;
    }

    public TrdtlPertandingan(TrdtlPertandinganPK trdtlPertandinganPK, Date menit) {
        this.trdtlPertandinganPK = trdtlPertandinganPK;
        this.menit = menit;
    }

    public TrdtlPertandingan(int dtsIdjad, int dtsIdpem) {
        this.trdtlPertandinganPK = new TrdtlPertandinganPK(dtsIdjad, dtsIdpem);
    }

    public TrdtlPertandinganPK getTrdtlPertandinganPK() {
        return trdtlPertandinganPK;
    }

    public void setTrdtlPertandinganPK(TrdtlPertandinganPK trdtlPertandinganPK) {
        this.trdtlPertandinganPK = trdtlPertandinganPK;
    }

    public Date getMenit() {
        return menit;
    }

    public void setMenit(Date menit) {
        this.menit = menit;
    }

    public Integer getDtsPelanggaran() {
        return dtsPelanggaran;
    }

    public void setDtsPelanggaran(Integer dtsPelanggaran) {
        this.dtsPelanggaran = dtsPelanggaran;
    }

    public Integer getDtsTangkap() {
        return dtsTangkap;
    }

    public void setDtsTangkap(Integer dtsTangkap) {
        this.dtsTangkap = dtsTangkap;
    }

    public Integer getDtsLolos() {
        return dtsLolos;
    }

    public void setDtsLolos(Integer dtsLolos) {
        this.dtsLolos = dtsLolos;
    }

    public MsPeserta getMsPeserta() {
        return msPeserta;
    }

    public void setMsPeserta(MsPeserta msPeserta) {
        this.msPeserta = msPeserta;
    }

    public TrJadwal getTrJadwal() {
        return trJadwal;
    }

    public void setTrJadwal(TrJadwal trJadwal) {
        this.trJadwal = trJadwal;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (trdtlPertandinganPK != null ? trdtlPertandinganPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TrdtlPertandingan)) {
            return false;
        }
        TrdtlPertandingan other = (TrdtlPertandingan) object;
        if ((this.trdtlPertandinganPK == null && other.trdtlPertandinganPK != null) || (this.trdtlPertandinganPK != null && !this.trdtlPertandinganPK.equals(other.trdtlPertandinganPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "polman.model.TrdtlPertandingan[ trdtlPertandinganPK=" + trdtlPertandinganPK + " ]";
    }
    
}
