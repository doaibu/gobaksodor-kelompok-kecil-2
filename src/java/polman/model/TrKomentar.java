/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polman.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Candra
 */
@Entity
@Table(name = "tr_Komentar")
@NamedQueries({
    @NamedQuery(name = "TrKomentar.findAll", query = "SELECT t FROM TrKomentar t")
    , @NamedQuery(name = "TrKomentar.findByKomId", query = "SELECT t FROM TrKomentar t WHERE t.komId = :komId")
    , @NamedQuery(name = "TrKomentar.findKomentarSatu", query = "select a.komBerita.berId,a.komId, a.komNama,a.komIsi from TrKomentar a where  a.komBerita.berId = :parent")
    , @NamedQuery(name = "TrKomentar.findByKomNama", query = "SELECT t FROM TrKomentar t WHERE t.komNama = :komNama")
    , @NamedQuery(name = "TrKomentar.findByKomEmail", query = "SELECT t FROM TrKomentar t WHERE t.komEmail = :komEmail")
    , @NamedQuery(name = "TrKomentar.findByKomIsi", query = "SELECT t FROM TrKomentar t WHERE t.komIsi = :komIsi")})
public class TrKomentar implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "kom_id",columnDefinition="int(1,1)")
    private Integer komId;
    @Size(max = 150)
    @Column(name = "kom_nama")
    private String komNama;
    @Size(max = 150)
    @Column(name = "kom_email")
    private String komEmail;
    @Size(max = 2147483647)
    @Column(name = "kom_isi")
    private String komIsi;
    @JoinColumn(name = "kom_berita", referencedColumnName = "ber_id")
    @ManyToOne
    private TrBerita komBerita;

    public TrKomentar() {
    }

    public TrKomentar(Integer komId) {
        this.komId = komId;
    }

    public Integer getKomId() {
        return komId;
    }

    public void setKomId(Integer komId) {
        this.komId = komId;
    }

    public String getKomNama() {
        return komNama;
    }

    public void setKomNama(String komNama) {
        this.komNama = komNama;
    }

    public String getKomEmail() {
        return komEmail;
    }

    public void setKomEmail(String komEmail) {
        this.komEmail = komEmail;
    }

    public String getKomIsi() {
        return komIsi;
    }

    public void setKomIsi(String komIsi) {
        this.komIsi = komIsi;
    }

    public TrBerita getKomBerita() {
        return komBerita;
    }

    public void setKomBerita(TrBerita komBerita) {
        this.komBerita = komBerita;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (komId != null ? komId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TrKomentar)) {
            return false;
        }
        TrKomentar other = (TrKomentar) object;
        if ((this.komId == null && other.komId != null) || (this.komId != null && !this.komId.equals(other.komId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "polman.model.TrKomentar[ komId=" + komId + " ]";
    }
    
}
