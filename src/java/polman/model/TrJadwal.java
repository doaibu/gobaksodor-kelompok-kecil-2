/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polman.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Candra
 */
@Entity
@Table(name = "tr_Jadwal")
@NamedQueries({
    @NamedQuery(name = "TrJadwal.findAll", query = "SELECT t FROM TrJadwal t")
    , @NamedQuery(name = "TrJadwal.findByJadId", query = "SELECT t FROM TrJadwal t WHERE t.jadId = :jadId")
    , @NamedQuery(name = "TrJadwal.findByJadTgl", query = "SELECT t FROM TrJadwal t WHERE t.jadTgl = :jadTgl")
    , @NamedQuery(name = "TrJadwal.findByJadJam", query = "SELECT t FROM TrJadwal t WHERE t.jadJam = :jadJam")
    , @NamedQuery(name = "TrJadwal.findByJadNilaiTandang", query = "SELECT t FROM TrJadwal t WHERE t.jadNilaiTandang = :jadNilaiTandang")
    , @NamedQuery(name = "TrJadwal.findByJadNilaiKandang", query = "SELECT t FROM TrJadwal t WHERE t.jadNilaiKandang = :jadNilaiKandang")
    , @NamedQuery(name = "TrJadwal.findByJadTimMenang", query = "SELECT t FROM TrJadwal t WHERE t.jadTimMenang = :jadTimMenang")
    , @NamedQuery(name = "TrJadwal.findByJadStatus", query = "SELECT t FROM TrJadwal t WHERE t.jadStatus = :jadStatus")
    , @NamedQuery(name = "TrJadwal.findByJadCreatedby", query = "SELECT t FROM TrJadwal t WHERE t.jadCreatedby = :jadCreatedby")
    , @NamedQuery(name = "TrJadwal.findByJadModivedby", query = "SELECT t FROM TrJadwal t WHERE t.jadModivedby = :jadModivedby")
    , @NamedQuery(name = "TrJadwal.findByJadCreateddate", query = "SELECT t FROM TrJadwal t WHERE t.jadCreateddate = :jadCreateddate")
    , @NamedQuery(name = "TrJadwal.findByJadModiveddate", query = "SELECT t FROM TrJadwal t WHERE t.jadModiveddate = :jadModiveddate")})
public class TrJadwal implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "jad_id")
    private Integer jadId;
    @Column(name = "jad_tgl")
    @Temporal(TemporalType.DATE)
    private Date jadTgl;
    @Column(name = "jad_jam")
    @Temporal(TemporalType.TIMESTAMP)
    private Date jadJam;
    @Column(name = "jad_nilai_tandang")
    private Integer jadNilaiTandang;
    @Column(name = "jad_nilai_kandang")
    private Integer jadNilaiKandang;
    @Column(name = "jad_tim_menang")
    private Integer jadTimMenang;
    @Column(name = "jad_status")
    private Integer jadStatus;
    @Size(max = 50)
    @Column(name = "jad_createdby")
    private String jadCreatedby;
    @Size(max = 50)
    @Column(name = "jad_modivedby")
    private String jadModivedby;
    @Column(name = "jad_createddate")
    @Temporal(TemporalType.DATE)
    private Date jadCreateddate;
    @Column(name = "jad_modiveddate")
    @Temporal(TemporalType.DATE)
    private Date jadModiveddate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "trJadwal")
    private Collection<TrdtlJadwalKru> trdtlJadwalKruCollection;
    @JoinColumn(name = "jad_lapangan", referencedColumnName = "lap_id")
    @ManyToOne(optional = false)
    private MsLapangan jadLapangan;
    @JoinColumn(name = "jad_tim_kandang", referencedColumnName = "tim_id")
    @ManyToOne
    private MsTim jadTimKandang;
    @JoinColumn(name = "jad_tim_tandang", referencedColumnName = "tim_id")
    @ManyToOne
    private MsTim jadTimTandang;
    @JoinColumn(name = "jad_cup_id", referencedColumnName = "cup_id")
    @ManyToOne
    private TrCup jadCupId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "trJadwal")
    private Collection<TrdtlPertandingan> trdtlPertandinganCollection;

    public TrJadwal() {
    }

    public TrJadwal(Integer jadId) {
        this.jadId = jadId;
    }

    public Integer getJadId() {
        return jadId;
    }

    public void setJadId(Integer jadId) {
        this.jadId = jadId;
    }

    public Date getJadTgl() {
        return jadTgl;
    }

    public void setJadTgl(Date jadTgl) {
        this.jadTgl = jadTgl;
    }

    public Date getJadJam() {
        return jadJam;
    }

    public void setJadJam(Date jadJam) {
        this.jadJam = jadJam;
    }

    public Integer getJadNilaiTandang() {
        return jadNilaiTandang;
    }

    public void setJadNilaiTandang(Integer jadNilaiTandang) {
        this.jadNilaiTandang = jadNilaiTandang;
    }

    public Integer getJadNilaiKandang() {
        return jadNilaiKandang;
    }

    public void setJadNilaiKandang(Integer jadNilaiKandang) {
        this.jadNilaiKandang = jadNilaiKandang;
    }

    public Integer getJadTimMenang() {
        return jadTimMenang;
    }

    public void setJadTimMenang(Integer jadTimMenang) {
        this.jadTimMenang = jadTimMenang;
    }

    public Integer getJadStatus() {
        return jadStatus;
    }

    public void setJadStatus(Integer jadStatus) {
        this.jadStatus = jadStatus;
    }

    public String getJadCreatedby() {
        return jadCreatedby;
    }

    public void setJadCreatedby(String jadCreatedby) {
        this.jadCreatedby = jadCreatedby;
    }

    public String getJadModivedby() {
        return jadModivedby;
    }

    public void setJadModivedby(String jadModivedby) {
        this.jadModivedby = jadModivedby;
    }

    public Date getJadCreateddate() {
        return jadCreateddate;
    }

    public void setJadCreateddate(Date jadCreateddate) {
        this.jadCreateddate = jadCreateddate;
    }

    public Date getJadModiveddate() {
        return jadModiveddate;
    }

    public void setJadModiveddate(Date jadModiveddate) {
        this.jadModiveddate = jadModiveddate;
    }

    public Collection<TrdtlJadwalKru> getTrdtlJadwalKruCollection() {
        return trdtlJadwalKruCollection;
    }

    public void setTrdtlJadwalKruCollection(Collection<TrdtlJadwalKru> trdtlJadwalKruCollection) {
        this.trdtlJadwalKruCollection = trdtlJadwalKruCollection;
    }

    public MsLapangan getJadLapangan() {
        return jadLapangan;
    }

    public void setJadLapangan(MsLapangan jadLapangan) {
        this.jadLapangan = jadLapangan;
    }

    public MsTim getJadTimKandang() {
        return jadTimKandang;
    }

    public void setJadTimKandang(MsTim jadTimKandang) {
        this.jadTimKandang = jadTimKandang;
    }

    public MsTim getJadTimTandang() {
        return jadTimTandang;
    }

    public void setJadTimTandang(MsTim jadTimTandang) {
        this.jadTimTandang = jadTimTandang;
    }

    public TrCup getJadCupId() {
        return jadCupId;
    }

    public void setJadCupId(TrCup jadCupId) {
        this.jadCupId = jadCupId;
    }

    public Collection<TrdtlPertandingan> getTrdtlPertandinganCollection() {
        return trdtlPertandinganCollection;
    }

    public void setTrdtlPertandinganCollection(Collection<TrdtlPertandingan> trdtlPertandinganCollection) {
        this.trdtlPertandinganCollection = trdtlPertandinganCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (jadId != null ? jadId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TrJadwal)) {
            return false;
        }
        TrJadwal other = (TrJadwal) object;
        if ((this.jadId == null && other.jadId != null) || (this.jadId != null && !this.jadId.equals(other.jadId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "polman.model.TrJadwal[ jadId=" + jadId + " ]";
    }
    
}
