/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polman.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Candra
 */
@Entity
@Table(name = "tr_Berita")
@NamedQueries({
    @NamedQuery(name = "TrBerita.findAll", query = "SELECT t FROM TrBerita t")
    , @NamedQuery(name = "TrBerita.findHL", query = "select a.berTglRilis,a.berJudul,a.berKonten,b.gbrGambar,a.berId,a.berResume from TrBerita a INNER JOIN TrGambarBerita b on a.berId = b.gbrBerita.berId AND b.gbrCaption = '1' group by a.berTglRilis , a.berJudul,a.berKonten,b.gbrGambar,a.berId,a.berResume")
    , @NamedQuery(name = "TrBerita.findPL", query = "select a.berTglRilis,a.berJudul,a.berId,a.berResume,b.gbrGambar,a.berPengunjung from TrBerita a INNER JOIN TrGambarBerita b on a.berId = b.gbrBerita.berId AND b.gbrCaption = '1'  group by a.berTglRilis , a.berJudul,a.berKonten,a.berId,a.berResume,b.gbrGambar,a.berPengunjung ORDER BY a.berPengunjung DESC")
    , @NamedQuery(name = "TrBerita.findUP", query = "select a.berId,a.berJudul,b.gbrGambar from TrBerita a,TrGambarBerita b where b.gbrBerita.berId = a.berId and b.gbrCaption = '1' and a.berTglRilis > :tgl")
    , @NamedQuery(name = "TrBerita.findByBerId", query = "SELECT t.berId, t.berJudul,T.berKonten, T.berTglRilis   FROM TrBerita t WHERE t.berId = :id")
    , @NamedQuery(name = "TrBerita.findKL", query = "select b.berId,b.berJudul,c.gbrGambar from TrKomentar a , TrBerita b, TrGambarBerita c where a.komBerita.berId = b.berId and b.berId = c.gbrBerita.berId and c.gbrCaption= '1' group by b.berId,b.berJudul,c.gbrGambar order by count(a.komBerita) desc")
    , @NamedQuery(name = "TrBerita.findByBerTglRilis", query = "SELECT t FROM TrBerita t WHERE t.berTglRilis = :berTglRilis")
    , @NamedQuery(name = "TrBerita.findByBerJudul", query = "SELECT t FROM TrBerita t WHERE t.berJudul = :berJudul")
    , @NamedQuery(name = "TrBerita.findByBerKonten", query = "SELECT t FROM TrBerita t WHERE t.berKonten = :berKonten")
    , @NamedQuery(name = "TrBerita.findByBerCreatedby", query = "SELECT t FROM TrBerita t WHERE t.berCreatedby = :berCreatedby")
    , @NamedQuery(name = "TrBerita.findByBerModivedby", query = "SELECT t FROM TrBerita t WHERE t.berModivedby = :berModivedby")
    , @NamedQuery(name = "TrBerita.findByBerCreateddate", query = "SELECT t FROM TrBerita t WHERE t.berCreateddate = :berCreateddate")
    , @NamedQuery(name = "TrBerita.findByBerModiveddate", query = "SELECT t FROM TrBerita t WHERE t.berModiveddate = :berModiveddate")
    , @NamedQuery(name = "TrBerita.findByBerResume", query = "SELECT t FROM TrBerita t WHERE t.berResume = :berResume")
    , @NamedQuery(name = "TrBerita.findByBerPengunjung", query = "SELECT t FROM TrBerita t WHERE t.berPengunjung = :berPengunjung")})
public class TrBerita implements Serializable {

    @OneToMany(mappedBy = "komBerita")
    private Collection<TrKomentar> trKomentarCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ber_id",columnDefinition="int(1,1)")
    private Integer berId;
    @Column(name = "ber_tgl_rilis")
    @Temporal(TemporalType.DATE)
    private Date berTglRilis;
    @Size(max = 2147483647)
    @Column(name = "ber_judul")
    private String berJudul;
    @Size(max = 2147483647)
    @Column(name = "ber_konten")
    private String berKonten;
    @Size(max = 50)
    @Column(name = "ber_createdby")
    private String berCreatedby;
    @Size(max = 50)
    @Column(name = "ber_modivedby")
    private String berModivedby;
    @Column(name = "ber_createddate")
    @Temporal(TemporalType.DATE)
    private Date berCreateddate;
    @Column(name = "ber_modiveddate")
    @Temporal(TemporalType.DATE)
    private Date berModiveddate;
    @Size(max = 2147483647)
    @Column(name = "ber_resume")
    private String berResume;
    @Column(name = "ber_pengunjung")
    private Integer berPengunjung;

    public TrBerita() {
    }

    public TrBerita(Integer berId) {
        this.berId = berId;
    }

    public Integer getBerId() {
        return berId;
    }

    public void setBerId(Integer berId) {
        this.berId = berId;
    }

    public Date getBerTglRilis() {
        return berTglRilis;
    }

    public void setBerTglRilis(Date berTglRilis) {
        this.berTglRilis = berTglRilis;
    }

    public String getBerJudul() {
        return berJudul;
    }

    public void setBerJudul(String berJudul) {
        this.berJudul = berJudul;
    }

    public String getBerKonten() {
        return berKonten;
    }

    public void setBerKonten(String berKonten) {
        this.berKonten = berKonten;
    }

    public String getBerCreatedby() {
        return berCreatedby;
    }

    public void setBerCreatedby(String berCreatedby) {
        this.berCreatedby = berCreatedby;
    }

    public String getBerModivedby() {
        return berModivedby;
    }

    public void setBerModivedby(String berModivedby) {
        this.berModivedby = berModivedby;
    }

    public Date getBerCreateddate() {
        return berCreateddate;
    }

    public void setBerCreateddate(Date berCreateddate) {
        this.berCreateddate = berCreateddate;
    }

    public Date getBerModiveddate() {
        return berModiveddate;
    }

    public void setBerModiveddate(Date berModiveddate) {
        this.berModiveddate = berModiveddate;
    }

    public String getBerResume() {
        return berResume;
    }

    public void setBerResume(String berResume) {
        this.berResume = berResume;
    }

    public Integer getBerPengunjung() {
        return berPengunjung;
    }

    public void setBerPengunjung(Integer berPengunjung) {
        this.berPengunjung = berPengunjung;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (berId != null ? berId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TrBerita)) {
            return false;
        }
        TrBerita other = (TrBerita) object;
        if ((this.berId == null && other.berId != null) || (this.berId != null && !this.berId.equals(other.berId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "polman.model.TrBerita[ berId=" + berId + " ]";
    }

    public Collection<TrKomentar> getTrKomentarCollection() {
        return trKomentarCollection;
    }

    public void setTrKomentarCollection(Collection<TrKomentar> trKomentarCollection) {
        this.trKomentarCollection = trKomentarCollection;
    }
    
}
