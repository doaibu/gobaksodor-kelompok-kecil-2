/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polman.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Candra
 */
@Entity
@Table(name = "tr_Cup")
@NamedQueries({
    @NamedQuery(name = "TrCup.findAll", query = "SELECT t FROM TrCup t")
    , @NamedQuery(name = "TrCup.findByCupId", query = "SELECT t FROM TrCup t WHERE t.cupId = :cupId")
    , @NamedQuery(name = "TrCup.findByCupNamakompetisi", query = "SELECT t FROM TrCup t WHERE t.cupNamakompetisi = :cupNamakompetisi")
    , @NamedQuery(name = "TrCup.findByCupPenyelenggara", query = "SELECT t FROM TrCup t WHERE t.cupPenyelenggara = :cupPenyelenggara")
    , @NamedQuery(name = "TrCup.findByCupMulai", query = "SELECT t FROM TrCup t WHERE t.cupMulai = :cupMulai")
    , @NamedQuery(name = "TrCup.findByCupAkhir", query = "SELECT t FROM TrCup t WHERE t.cupAkhir = :cupAkhir")
    , @NamedQuery(name = "TrCup.findByCupCreatedby", query = "SELECT t FROM TrCup t WHERE t.cupCreatedby = :cupCreatedby")
    , @NamedQuery(name = "TrCup.findByCupModivedby", query = "SELECT t FROM TrCup t WHERE t.cupModivedby = :cupModivedby")
    , @NamedQuery(name = "TrCup.findByCupCreateddate", query = "SELECT t FROM TrCup t WHERE t.cupCreateddate = :cupCreateddate")
    , @NamedQuery(name = "TrCup.findByCupModiveddate", query = "SELECT t FROM TrCup t WHERE t.cupModiveddate = :cupModiveddate")})
public class TrCup implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
     @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cup_id",columnDefinition="int(1,1)")
    private Integer cupId;
    @Size(max = 100)
    @Column(name = "cup_namakompetisi")
    private String cupNamakompetisi;
    @Size(max = 50)
    @Column(name = "cup_penyelenggara")
    private String cupPenyelenggara;
    @Column(name = "cup_mulai")
    @Temporal(TemporalType.DATE)
    private Date cupMulai;
    @Column(name = "cup_akhir")
    @Temporal(TemporalType.DATE)
    private Date cupAkhir;
    @Size(max = 50)
    @Column(name = "cup_createdby")
    private String cupCreatedby;
    @Size(max = 50)
    @Column(name = "cup_modivedby")
    private String cupModivedby;
    @Column(name = "cup_createddate")
    @Temporal(TemporalType.DATE)
    private Date cupCreateddate;
    @Column(name = "cup_modiveddate")
    @Temporal(TemporalType.DATE)
    private Date cupModiveddate;
    @OneToMany(mappedBy = "jadCupId")
    private Collection<TrJadwal> trJadwalCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "trCup")
    private Collection<TrdtlPrestasiPemain> trdtlPrestasiPemainCollection;

    public TrCup() {
    }

    public TrCup(Integer cupId) {
        this.cupId = cupId;
    }

    public Integer getCupId() {
        return cupId;
    }

    public void setCupId(Integer cupId) {
        this.cupId = cupId;
    }

    public String getCupNamakompetisi() {
        return cupNamakompetisi;
    }

    public void setCupNamakompetisi(String cupNamakompetisi) {
        this.cupNamakompetisi = cupNamakompetisi;
    }

    public String getCupPenyelenggara() {
        return cupPenyelenggara;
    }

    public void setCupPenyelenggara(String cupPenyelenggara) {
        this.cupPenyelenggara = cupPenyelenggara;
    }

    public Date getCupMulai() {
        return cupMulai;
    }

    public void setCupMulai(Date cupMulai) {
        this.cupMulai = cupMulai;
    }

    public Date getCupAkhir() {
        return cupAkhir;
    }

    public void setCupAkhir(Date cupAkhir) {
        this.cupAkhir = cupAkhir;
    }

    public String getCupCreatedby() {
        return cupCreatedby;
    }

    public void setCupCreatedby(String cupCreatedby) {
        this.cupCreatedby = cupCreatedby;
    }

    public String getCupModivedby() {
        return cupModivedby;
    }

    public void setCupModivedby(String cupModivedby) {
        this.cupModivedby = cupModivedby;
    }

    public Date getCupCreateddate() {
        return cupCreateddate;
    }

    public void setCupCreateddate(Date cupCreateddate) {
        this.cupCreateddate = cupCreateddate;
    }

    public Date getCupModiveddate() {
        return cupModiveddate;
    }

    public void setCupModiveddate(Date cupModiveddate) {
        this.cupModiveddate = cupModiveddate;
    }

    public Collection<TrJadwal> getTrJadwalCollection() {
        return trJadwalCollection;
    }

    public void setTrJadwalCollection(Collection<TrJadwal> trJadwalCollection) {
        this.trJadwalCollection = trJadwalCollection;
    }

    public Collection<TrdtlPrestasiPemain> getTrdtlPrestasiPemainCollection() {
        return trdtlPrestasiPemainCollection;
    }

    public void setTrdtlPrestasiPemainCollection(Collection<TrdtlPrestasiPemain> trdtlPrestasiPemainCollection) {
        this.trdtlPrestasiPemainCollection = trdtlPrestasiPemainCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cupId != null ? cupId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TrCup)) {
            return false;
        }
        TrCup other = (TrCup) object;
        if ((this.cupId == null && other.cupId != null) || (this.cupId != null && !this.cupId.equals(other.cupId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "polman.model.TrCup[ cupId=" + cupId + " ]";
    }
    
}
