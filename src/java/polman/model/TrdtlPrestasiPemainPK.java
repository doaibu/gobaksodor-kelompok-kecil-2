/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polman.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Candra
 */
@Embeddable
public class TrdtlPrestasiPemainPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "dtp_idcup")
    private int dtpIdcup;
    @Basic(optional = false)
    @NotNull
    @Column(name = "dtp_idpem")
    private int dtpIdpem;

    public TrdtlPrestasiPemainPK() {
    }

    public TrdtlPrestasiPemainPK(int dtpIdcup, int dtpIdpem) {
        this.dtpIdcup = dtpIdcup;
        this.dtpIdpem = dtpIdpem;
    }

    public int getDtpIdcup() {
        return dtpIdcup;
    }

    public void setDtpIdcup(int dtpIdcup) {
        this.dtpIdcup = dtpIdcup;
    }

    public int getDtpIdpem() {
        return dtpIdpem;
    }

    public void setDtpIdpem(int dtpIdpem) {
        this.dtpIdpem = dtpIdpem;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) dtpIdcup;
        hash += (int) dtpIdpem;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TrdtlPrestasiPemainPK)) {
            return false;
        }
        TrdtlPrestasiPemainPK other = (TrdtlPrestasiPemainPK) object;
        if (this.dtpIdcup != other.dtpIdcup) {
            return false;
        }
        if (this.dtpIdpem != other.dtpIdpem) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "polman.model.TrdtlPrestasiPemainPK[ dtpIdcup=" + dtpIdcup + ", dtpIdpem=" + dtpIdpem + " ]";
    }
    
}
