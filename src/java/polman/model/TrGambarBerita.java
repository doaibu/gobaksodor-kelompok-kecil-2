/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polman.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Candra
 */
@Entity
@Table(name = "tr_GambarBerita")
@NamedQueries({
    @NamedQuery(name = "TrGambarBerita.findAll", query = "SELECT t FROM TrGambarBerita t")
    , @NamedQuery(name = "TrGambarBerita.findByGbrId", query = "SELECT t FROM TrGambarBerita t WHERE t.gbrId = :gbrId")
    , @NamedQuery(name = "TrGambarBerita.findByIdBerita", query = "SELECT t.gbrGambar FROM TrGambarBerita t WHERE t.gbrBerita.berId = :id")
    , @NamedQuery(name = "TrGambarBerita.findByGbrGambar", query = "SELECT t FROM TrGambarBerita t WHERE t.gbrGambar = :gbrGambar")
    , @NamedQuery(name = "TrGambarBerita.findByGbrCaption", query = "SELECT t FROM TrGambarBerita t WHERE t.gbrCaption = :gbrCaption")})
public class TrGambarBerita implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "gbr_id",columnDefinition="int(1,1)")
    private Integer gbrId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "gbr_gambar")
    private String gbrGambar;
    @Size(max = 2147483647)
    @Column(name = "gbr_caption")
    private String gbrCaption;
    @JoinColumn(name = "gbr_berita", referencedColumnName = "ber_id")
    @ManyToOne(optional = false)
    private TrBerita gbrBerita;

    public TrGambarBerita() {
    }

    public TrGambarBerita(Integer gbrId) {
        this.gbrId = gbrId;
    }

    public TrGambarBerita(Integer gbrId, String gbrGambar) {
        this.gbrId = gbrId;
        this.gbrGambar = gbrGambar;
    }

    public Integer getGbrId() {
        return gbrId;
    }

    public void setGbrId(Integer gbrId) {
        this.gbrId = gbrId;
    }

    public String getGbrGambar() {
        return gbrGambar;
    }

    public void setGbrGambar(String gbrGambar) {
        this.gbrGambar = gbrGambar;
    }

    public String getGbrCaption() {
        return gbrCaption;
    }

    public void setGbrCaption(String gbrCaption) {
        this.gbrCaption = gbrCaption;
    }

    public TrBerita getGbrBerita() {
        return gbrBerita;
    }

    public void setGbrBerita(TrBerita gbrBerita) {
        this.gbrBerita = gbrBerita;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gbrId != null ? gbrId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TrGambarBerita)) {
            return false;
        }
        TrGambarBerita other = (TrGambarBerita) object;
        if ((this.gbrId == null && other.gbrId != null) || (this.gbrId != null && !this.gbrId.equals(other.gbrId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "polman.model.TrGambarBerita[ gbrId=" + gbrId + " ]";
    }
    
}
