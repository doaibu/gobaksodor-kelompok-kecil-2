/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polman.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Candra
 */
@Entity
@Table(name = "ms_Tim")
@NamedQueries({
    @NamedQuery(name = "MsTim.findAll", query = "SELECT m FROM MsTim m")
    , @NamedQuery(name = "MsTim.findByTimId", query = "SELECT m FROM MsTim m WHERE m.timId = :timId")
    , @NamedQuery(name = "MsTim.findByTimEmail", query = "SELECT m FROM MsTim m WHERE m.timEmail = :timEmail")
    , @NamedQuery(name = "MsTim.findByTimPassword", query = "SELECT m FROM MsTim m WHERE m.timPassword = :timPassword")
    , @NamedQuery(name = "MsTim.findByTimNama", query = "SELECT m FROM MsTim m WHERE m.timNama = :timNama")
    , @NamedQuery(name = "MsTim.findByTimLambang", query = "SELECT m FROM MsTim m WHERE m.timLambang = :timLambang")
    , @NamedQuery(name = "MsTim.findByTimCreateby", query = "SELECT m FROM MsTim m WHERE m.timCreateby = :timCreateby")
    , @NamedQuery(name = "MsTim.findByTimModivedby", query = "SELECT m FROM MsTim m WHERE m.timModivedby = :timModivedby")
    , @NamedQuery(name = "MsTim.findByTimCreateddate", query = "SELECT m FROM MsTim m WHERE m.timCreateddate = :timCreateddate")
    , @NamedQuery(name = "MsTim.findByTimModiveddate", query = "SELECT m FROM MsTim m WHERE m.timModiveddate = :timModiveddate")})
public class MsTim implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "tim_id")
    private Integer timId;
    @Size(max = 50)
    @Column(name = "tim_email")
    private String timEmail;
    @Size(max = 50)
    @Column(name = "tim_password")
    private String timPassword;
    @Size(max = 50)
    @Column(name = "tim_nama")
    private String timNama;
    @Size(max = 150)
    @Column(name = "tim_lambang")
    private String timLambang;
    @Size(max = 50)
    @Column(name = "tim_createby")
    private String timCreateby;
    @Size(max = 50)
    @Column(name = "tim_modivedby")
    private String timModivedby;
    @Column(name = "tim_createddate")
    @Temporal(TemporalType.DATE)
    private Date timCreateddate;
    @Column(name = "tim_modiveddate")
    @Temporal(TemporalType.DATE)
    private Date timModiveddate;
    @OneToMany(mappedBy = "jadTimKandang")
    private Collection<TrJadwal> trJadwalCollection;
    @OneToMany(mappedBy = "jadTimTandang")
    private Collection<TrJadwal> trJadwalCollection1;
    @OneToMany(mappedBy = "pesIdTim")
    private Collection<MsPeserta> msPesertaCollection;

    public MsTim() {
    }

    public MsTim(Integer timId) {
        this.timId = timId;
    }

    public Integer getTimId() {
        return timId;
    }

    public void setTimId(Integer timId) {
        this.timId = timId;
    }

    public String getTimEmail() {
        return timEmail;
    }

    public void setTimEmail(String timEmail) {
        this.timEmail = timEmail;
    }

    public String getTimPassword() {
        return timPassword;
    }

    public void setTimPassword(String timPassword) {
        this.timPassword = timPassword;
    }

    public String getTimNama() {
        return timNama;
    }

    public void setTimNama(String timNama) {
        this.timNama = timNama;
    }

    public String getTimLambang() {
        return timLambang;
    }

    public void setTimLambang(String timLambang) {
        this.timLambang = timLambang;
    }

    public String getTimCreateby() {
        return timCreateby;
    }

    public void setTimCreateby(String timCreateby) {
        this.timCreateby = timCreateby;
    }

    public String getTimModivedby() {
        return timModivedby;
    }

    public void setTimModivedby(String timModivedby) {
        this.timModivedby = timModivedby;
    }

    public Date getTimCreateddate() {
        return timCreateddate;
    }

    public void setTimCreateddate(Date timCreateddate) {
        this.timCreateddate = timCreateddate;
    }

    public Date getTimModiveddate() {
        return timModiveddate;
    }

    public void setTimModiveddate(Date timModiveddate) {
        this.timModiveddate = timModiveddate;
    }

    public Collection<TrJadwal> getTrJadwalCollection() {
        return trJadwalCollection;
    }

    public void setTrJadwalCollection(Collection<TrJadwal> trJadwalCollection) {
        this.trJadwalCollection = trJadwalCollection;
    }

    public Collection<TrJadwal> getTrJadwalCollection1() {
        return trJadwalCollection1;
    }

    public void setTrJadwalCollection1(Collection<TrJadwal> trJadwalCollection1) {
        this.trJadwalCollection1 = trJadwalCollection1;
    }

    public Collection<MsPeserta> getMsPesertaCollection() {
        return msPesertaCollection;
    }

    public void setMsPesertaCollection(Collection<MsPeserta> msPesertaCollection) {
        this.msPesertaCollection = msPesertaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (timId != null ? timId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MsTim)) {
            return false;
        }
        MsTim other = (MsTim) object;
        if ((this.timId == null && other.timId != null) || (this.timId != null && !this.timId.equals(other.timId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "polman.model.MsTim[ timId=" + timId + " ]";
    }
    
}
